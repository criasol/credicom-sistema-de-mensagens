﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MensagensNotificador.Editor;
using MensagensNotificador.Views;

namespace MensagensNotificador
{
    public partial class EditorForm : Form
    {
        private string _filename = null;

        public EditorForm()
        {
            InitializeComponent();
            radioButtonTexto.Checked = true;
            editor.Tick += new Editor.Editor.TickDelegate(editor_Tick);
        }

        private void editor_Tick()
        {
            undoToolStripMenuItem.Enabled = editor.CanUndo();
            redoToolStripMenuItem.Enabled = editor.CanRedo();
            cutToolStripMenuItem.Enabled = editor.CanCut();
            copyToolStripMenuItem.Enabled = editor.CanCopy();
            pasteToolStripMenuItem.Enabled = editor.CanPaste();

            editor.undoButton.Enabled = editor.CanUndo();
            editor.redoButton.Enabled = editor.CanRedo();
            editor.cutButton.Enabled = editor.CanCut();
            editor.copyButton.Enabled = editor.CanCopy();
            editor.pasteButton.Enabled = editor.CanPaste();
        }


        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SearchDialog dlg = new SearchDialog(editor))
            {
                dlg.ShowDialog(this);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.SelectAll();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Paste();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.Redo();
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, editor.BodyText);
        }

        private void htmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, editor.BodyHtml);
        }

        private void breakToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editor.InsertBreak();
        }

        private void textToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (TextInsertForm form = new TextInsertForm(editor.BodyHtml))
            {
                form.ShowDialog(this);
                if (form.Accepted)
                {
                    editor.BodyHtml = form.HTML;
                }
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            if (mensagemOk())
            {
                if (radioButtonLink.Checked)
                {
                    FrmMostraMensagem frmMostraMensagem = new FrmMostraMensagem(true, txtLinkMensagem.Text);
                    frmMostraMensagem.Show();
                }
                else
                {
                    FrmMostraMensagem frmMostraMensagem = new FrmMostraMensagem(false, editor.BodyHtml);
                    frmMostraMensagem.Show();
                }
            }

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            FormPrincipal.Envia_Mensagem(editor.BodyHtml);
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void radioButtonTexto_CheckedChanged(object sender, EventArgs e)
        {
            txtLinkMensagem.Enabled = false;
            editor.Enabled = true;
        }

        private void radioButtonLink_CheckedChanged(object sender, EventArgs e)
        {
            txtLinkMensagem.Enabled = true;
            editor.Enabled = false;
        }

        private bool mensagemOk()
        {
            bool result = true;
            Uri uri;
            string url;
            if (radioButtonLink.Checked)
            {
                if (txtLinkMensagem.Text.Trim() == "")
                {
                    MessageBox.Show("Informe o link da mensagem.", "Erro",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    result = false;
                }
            }
            else
            {
                if (editor.BodyHtml.Trim() == "")
                {
                    MessageBox.Show("Informe o texto da mensagem.", "Erro",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    result = false;
                }
            }
            return result;
        }
    }
}
