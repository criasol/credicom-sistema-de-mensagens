﻿namespace AgenteNotificador
{
    partial class frmListaMensagens
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lsvMensagens = new System.Windows.Forms.ListView();
            this.btnSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lsvMensagens
            // 
            this.lsvMensagens.Location = new System.Drawing.Point(25, 36);
            this.lsvMensagens.Name = "lsvMensagens";
            this.lsvMensagens.Size = new System.Drawing.Size(919, 342);
            this.lsvMensagens.TabIndex = 0;
            this.lsvMensagens.UseCompatibleStateImageBehavior = false;
            this.lsvMensagens.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lsvMensagens_MouseDoubleClick);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Location = new System.Drawing.Point(421, 393);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(75, 38);
            this.btnSair.TabIndex = 3;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // frmListaMensagens
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 443);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.lsvMensagens);
            this.Name = "frmListaMensagens";
            this.Text = "frmListaMensagens";
            this.Load += new System.EventHandler(this.frmListaMensagens_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lsvMensagens;
        private System.Windows.Forms.Button btnSair;
    }
}