﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System.Reflection;
using Microsoft.Owin;


[assembly: OwinStartup(typeof(ServicoNotificador.Startup))]
namespace ServicoNotificador
{
    public partial class ServicoNotificadorMensagens : ServiceBase
    {

        private IDisposable SignalR { get; set; }
        const string ServerURI = "http://localhost:8080";

        private System.Diagnostics.EventLog logEventos;

        private int eventId = 0;

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public ServicoNotificadorMensagens()
        {
            InitializeComponent();
            logEventos = new EventLog();
            if (!EventLog.SourceExists("ServicoNotificador"))
            {
                EventLog.CreateEventSource(
                    "ServicoNotificador", "LogNotificador");
            }
            logEventos.Source = "ServicoNotificador";
            logEventos.Log = "LogNotificador";
        }

        protected override void OnStart(string[] args)
        {
            logEventos.WriteEntry("Iniciando o serviço de notificação de mensagens");

            // Update the service state to Start Pending.  
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Set up a timer to trigger every minute.  
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000; // 60 seconds  
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.  
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
            // Iniciando o serviço SignalR
            try
            {
                SignalR = WebApp.Start(ServerURI);
            }
            catch (TargetInvocationException)
            {
                logEventos.WriteEntry("Serviço de notificação de mensagens (SignalR) não iniciado. Existe um serviço ja iniciado em " + ServerURI);
            }
            logEventos.WriteEntry("Serviço de notificação de mensagens (SignalR) iniciado em " + ServerURI);
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.  
            logEventos.WriteEntry("Monitorando o sistema ", EventLogEntryType.Information, eventId++);
        }

        protected override void OnStop()
        {
            logEventos.WriteEntry("Parando o serviço de notificação de mensagens");
            SignalR.Dispose();
        }

        protected override void OnContinue()
        {
            logEventos.WriteEntry("Continuando o serviço de notificação de mensagens (SignalR)");
            // Reiniciando o serviço SignalR
            try
            {
                SignalR = WebApp.Start(ServerURI);
            }
            catch (TargetInvocationException)
            {
                logEventos.WriteEntry("Serviço de notificação de mensagens (SignalR) não reiniciado. Existe um serviço ja iniciado em " + ServerURI);
            }
            logEventos.WriteEntry("Serviço de notificação de mensagens (SignalR) reiniciado em " + ServerURI);

        }

    }


    // ////////////////////////////
    //
    // Server SignalR
    //
    // //////////////////////////// 

    /// 
    /// Used by OWIN's startup process. 
    /// 
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }

    /// 
    /// Echoes messages sent using the Send message by calling the
    /// addMessage method on the client. Also reports to the console
    /// when clients connect and disconnect.
    /// 

    public class MyHub : Hub
    {
        private EventLog logEventos = new EventLog();

        public void Send(string name, string message)
        {

            Clients.All.addMessage(name, message);
            //logEventos.WriteEntry("Mensagem Enviada : " + Context.ConnectionId + ": " + name + ": " + message);
        }

        public override async Task OnDisconnected(bool stopCalled)
        {
            await base.OnDisconnected(stopCalled);
            //logEventos.WriteEntry("Client disconnected");
        }

        public override async Task OnConnected()
        {
            await base.OnConnected();
            grava_log("Conexão ok");
        }

        public override async Task OnReconnected()
        {
            await base.OnReconnected();
            //logEventos.WriteEntry("Client reconnected: ");
        }

        public void grava_log(string msg)
        {
            if (!EventLog.SourceExists("ServicoNotificador"))
            {
                EventLog.CreateEventSource(
                    "ServicoNotificador", "LogNotificador");
            }
            logEventos.Source = "ServicoNotificador";
            logEventos.Log = "LogNotificador";
            try
            {
                logEventos.WriteEntry(msg);
            }
            catch (Exception e)
            {
                using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(@"C:\temp\WriteLines2.txt", true))
                {
                    file.WriteLine(e.Message);
                }
            }
        }

    }
}
