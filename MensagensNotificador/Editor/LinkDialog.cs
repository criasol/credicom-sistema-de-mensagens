﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace MensagensNotificador.Editor
{
    public partial class LinkDialog : Form
    {
        private bool _accepted = false;

        public LinkDialog()
        {
            InitializeComponent();
            linkEdit.TextChanged += new EventHandler(linkEdit_TextChanged);
        }

        void linkEdit_TextChanged(object sender, EventArgs e)
        {
            label1.Text = comboBox1.Text + linkEdit.Text;
        }

        public string URL
        {
            get
            {
                return linkEdit.Text.Trim();
            }
            set
            {
                linkEdit.Text = value;
            }
        }

        public string TituloURL
        {
            get
            {
                return txtTexto.Text.Trim();
            }
            set
            {
                txtTexto.Text = value;
            }
        }
        public string Scheme
        {
            get
            {
                return comboBox1.Text;
            }
            set
            {
                comboBox1.Text = value;
            }
        }

        public bool Accepted
        {
            get
            {
                return _accepted;
            }
        }

        private void LinkDialog_Load(object sender, EventArgs e)
        {
            label1.Text = comboBox1.Text + URL;
            BeginInvoke((MethodInvoker)delegate
            {
                linkEdit.Focus();
            });
        }


        private void OKButton_Click(object sender, EventArgs e)
        {
            string url = linkEdit.Text.Trim();
            string texto =  txtTexto.Text.Trim();
            if (texto.Length > 0 & url.Length > 0)
            {
                _accepted = true;
                Close();
            }
            else if (url.Length == 0)
            {
                MessageBox.Show("Informe a url referente ao hiperlink.", "Erro",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            } else
            {
                MessageBox.Show("Informe o texto referente ao hiperlink.", "Erro",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            _accepted = false;
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Text = comboBox1.Text + URL;
        }
    }
}
