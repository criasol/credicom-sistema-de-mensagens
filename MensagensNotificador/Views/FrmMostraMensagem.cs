﻿using System;
using System.Windows.Forms;

namespace MensagensNotificador.Views
{
    public partial class FrmMostraMensagem : Form
    {
        public FrmMostraMensagem(bool ehLink, string link_html)
        {
            InitializeComponent();
            if (ehLink)
            {
                this.webBrowser1.Navigate(link_html);
            }
            else
            {
                this.webBrowser1.DocumentText = link_html;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
