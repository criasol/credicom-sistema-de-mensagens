﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgenteNotificador
{
    public class Postagem
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
        public bool Lido { get; set; }
    }
}
