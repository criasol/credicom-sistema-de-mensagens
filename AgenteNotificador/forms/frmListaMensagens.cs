﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AgenteNotificador
{
    public partial class frmListaMensagens : Form
    {
        public List<Postagem> postagens;

        public frmListaMensagens(List<Postagem> Postagens)
        {
            postagens = Postagens;
            InitializeComponent();
        }

        private void frmListaMensagens_Load(object sender, EventArgs e)
        {
            lsvMensagens.View = View.Details;
            lsvMensagens.GridLines = true;
            lsvMensagens.FullRowSelect = true;

            //Add column header
            lsvMensagens.Columns.Add("Mensagem", 50);
            lsvMensagens.Columns.Add("Título", 400);
            lsvMensagens.Columns.Add("Texto",400);
            lsvMensagens.Columns.Add("Lido", 40);

            //Add items in the listview
            string[] arr = new string[4];
            ListViewItem itm;

            foreach (Postagem postagem in postagens)
            {
                arr[0] = postagem.Id.ToString();
                arr[1] = postagem.Titulo;
                arr[2] = postagem.Mensagem;
                if (postagem.Lido)
                {
                    arr[3] = "Sim";
                }
                else
                {
                    arr[3] = "Não";
                }
                itm = new ListViewItem(arr);
                lsvMensagens.Items.Add(itm);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
           Close();
        }

        private void lsvMensagens_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Debug.WriteLine("aaaaa");
        }
    }
}
