﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Windows.Forms;

namespace AgenteNotificador
{
    /// <summary>
    /// SignalR client hosted in a WinForms application. The client
    /// lets the user pick a user name, connect to the server asynchronously
    /// to not block the UI thread, and send chat messages to all connected 
    /// clients whether they are hosted in WinForms, WPF, or a web application.
    /// </summary>
    public partial class frmAgenteNotificador : Form
    {
        /// <summary>
        /// This name is simply added to sent messages to identify the user; this 
        /// sample does not include authentication.
        /// </summary>
        private String UserName { get; set; }
        private IHubProxy HubProxy { get; set; }
        const string ServerURI = "http://localhost:8080/signalr";
        private HubConnection Connection { get; set; }

        private bool allowVisible;     
        private bool allowClose;
        
        public List<Postagem> postagens = new List<Postagem>();
        public int inx = 0;

        internal frmAgenteNotificador()
        {
            InitializeComponent();

            UserName = Environment.UserName;
            //Connect to server (use async method to avoid blocking UI thread)
            StatusText.Visible = true;
            StatusText.Text = "Conectando no Servidor...";
            ConnectAsync();
           
        }
        protected override void SetVisibleCore(bool value)
        {
            if (!allowVisible)
            {
                value = false;
                if (!this.IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!allowClose)
            {
                this.Hide();
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }

        private void ButtonSend_Click(object sender, EventArgs e)
        {
            HubProxy.Invoke("Send", UserName, TextBoxMessage.Text);
            TextBoxMessage.Text = String.Empty;
            TextBoxMessage.Focus();
        }

        /// <summary>
        /// Creates and connects the hub connection and hub proxy. This method
        /// is called asynchronously from SignInButton_Click.
        /// </summary>
        private async void ConnectAsync()
        {
            Connection = new HubConnection(ServerURI);
            Connection.Closed += Connection_Closed;
            HubProxy = Connection.CreateHubProxy("MyHub");
            //Handle incoming event from server: use Invoke to write to console from SignalR's thread
            HubProxy.On<string, string>("AddMessage", (name, message) =>
                this.Invoke((Action)(() =>
                    {
                        int chave = incluiMensagem(name, message);
                        RichTextBoxConsole.AppendText(String.Format("{0}: {1}" + Environment.NewLine, name, message));
                        iconeNotificacao.Text = chave.ToString();
                        iconeNotificacao.BalloonTipTitle = name;
                        iconeNotificacao.BalloonTipText = message;
                        iconeNotificacao.BalloonTipIcon = ToolTipIcon.Info;
                        iconeNotificacao.ShowBalloonTip(2000);
                    }
                ))
            );
            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException)
            {
                StatusText.Text = "Não foi possível conectar ao servidor: É necessário que o servidor esteja iniciado previamente.";
                //No connection: Don't enable Send button or show chat UI
                return;
            }

            //Activate UI
            SignInPanel.Visible = false;
            ChatPanel.Visible = true;
            ButtonSend.Enabled = true;
            TextBoxMessage.Focus();
            RichTextBoxConsole.AppendText("Conectado ao servidor: " + ServerURI + Environment.NewLine);
        }

        /// <summary>
        /// If the server is stopped, the connection will time out after 30 seconds (default), and the 
        /// Closed event will fire.
        /// </summary>
        private void Connection_Closed()
        {
            //Deactivate chat UI; show login UI. 
            this.Invoke((Action)(() => ChatPanel.Visible = false));
            this.Invoke((Action)(() => ButtonSend.Enabled = false));
            this.Invoke((Action)(() => StatusText.Text = "O programa foi desconectado do servidor."));
            this.Invoke((Action)(() => SignInPanel.Visible = true));
        }

        private void SignInButton_Click(object sender, EventArgs e)
        {
            UserName = UserNameTextBox.Text;
            //Connect to server (use async method to avoid blocking UI thread)
            if (!String.IsNullOrEmpty(UserName))
            {
                StatusText.Visible = true;
                StatusText.Text = "Conectando no Servidor...";
                ConnectAsync();
            }
        }

        private void AgenteNotificador_Move(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allowVisible = true;
            frmListaMensagens listaMensagens = new frmListaMensagens(postagens);
            listaMensagens.ShowDialog();
        }


        private void testeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            mostra_Notificacao(0, "Teste de Notificação", 
                                    "Notificaçao Ok. Clique para ver Mensagem de Teste. http://criasol.com.br",
                                    ToolTipIcon.Info);
        }

        private void iconeNotificacao_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            allowVisible = true;
            frmListaMensagens listaMensagens = new frmListaMensagens(postagens);
            listaMensagens.ShowDialog();
        }

        private void iconeNotificacao_BalloonTipClicked(object sender, EventArgs e)
        {
            allowVisible = true;
            frmExibeMensagem mensagem = new frmExibeMensagem();
            Debug.WriteLine(iconeNotificacao.BalloonTipTitle);
            mensagem.lblMensagem.Text = iconeNotificacao.BalloonTipTitle;
            mensagem.webBrowser1.DocumentText = iconeNotificacao.BalloonTipText;

            mensagem.ShowDialog();
        }

        private void btnMensagens_Click(object sender, EventArgs e)
        {
            frmListaMensagens listaMensagens = new frmListaMensagens(postagens);
            listaMensagens.ShowDialog();

        }

        private void mostra_Notificacao(int chave, 
                                        string titulo, 
                                        string texto,
                                        ToolTipIcon icone)
        {
            iconeNotificacao.Text = chave.ToString();
            iconeNotificacao.BalloonTipTitle = titulo;
            iconeNotificacao.BalloonTipText = texto;
            iconeNotificacao.BalloonTipIcon = icone;

            iconeNotificacao.ShowBalloonTip(2000);
        }

        private int incluiMensagem(string titulo, string texto)
        {
            inx += 1;
            Postagem postagem= new Postagem();
            postagem.Id = inx;
            postagem.Titulo = titulo;
            postagem.Mensagem = texto;
            postagem.Lido = false;
            postagens.Add(postagem);
            return inx;
        }
    }
}
